#include <SoftwareSerial.h>
#define fsrpinB A0
#define fsrpinF A1
#define fsrpinE A4
#define fsrpinC A2
#define fsrpinD A3
#define fsrpinA A5

SoftwareSerial btSerial(2, 3); // RX, TX

int btStatePin = 4;
bool btState = false;
bool cmdStop = false;
long count = 0;
int fsrA, fsrB, fsrC, fsrD, fsrE, fsrF; 

void setup() {
  Serial.begin(38400);
  pinMode(btStatePin,INPUT);
  btSerial.begin(38400);
  btSerial.write("Initialized");
}

void loop() {

//bluetooth state
  btState = digitalRead(btStatePin);
//  Serial.print("Bluetooth State: ");
//  Serial.println(btState);
  
//HC-05 chat
  if (btSerial.available()){
    char btReceiver = btSerial.read();
    Serial.write(btReceiver);
    cmdStop = true;
    if (cmdStop && btReceiver == 'Y'){
      cmdStop = false;
    }
  }
  if (Serial.available())
    btSerial.write(Serial.read());

//read FSR data
  fsrA = analogRead(fsrpinA);
  fsrB = analogRead(fsrpinB);
  fsrC = analogRead(fsrpinC);
  fsrD = analogRead(fsrpinD);
  fsrE = analogRead(fsrpinE);
  fsrF = analogRead(fsrpinF);

  Serial.print("CalA= ");
  Serial.print(calibration(fsrA, 'A'));
  Serial.print(", CalB= ");
  Serial.print(calibration(fsrB, 'B'));
  Serial.print(", CalC= ");
  Serial.print(calibration(fsrC, 'C'));
  Serial.print(", CalD= ");
  Serial.print(calibration(fsrD, 'D')); 
  Serial.print(", CalE= ");
  Serial.print(calibration(fsrE, 'E'));
  Serial.print(", CalF= ");
  Serial.println(calibration(fsrF, 'F'));
//  printSignal();

  //print data through bluetooth only if connection has been established
  //note that btState = 0 even if HC-05 has paired with phone/other devices
  if (btState && !cmdStop){ 
    //btSerial.write("##");
    printSignal();
  }
  
 // delay(10);
}

//convert raw data to calibrated data
float calibration (int reading, char sensor){
  float V_out = reading*5.0/1023.0;
  float resistance = 100000;
  float conductance = V_out/(resistance*(5 - V_out));
  
  switch (sensor){
    case 'A':
      return 6*(pow(10, 12))*conductance*conductance + 3*(pow(10, 8))*conductance;
      break;  
    case 'B':
      return 6*(pow(10, 12))*conductance*conductance + 4*(pow(10, 8))*conductance;
      break;
    case 'C':
    return 5*(pow(10, 8))*conductance;
//      return 3*(pow(10, 12))*conductance*conductance + 1*(pow(10, 8))*conductance;
      break; 
    case 'D':
      return 2*(pow(10, 12))*conductance*conductance + 5*(pow(10, 8))*conductance;
      break; 
    case 'E':
      return 2*(pow(10, 12))*conductance*conductance + 7*(pow(10, 8))*conductance;
      break;            
    case 'F':
      return 4*(pow(10, 12))*conductance*conductance + 4*(pow(10, 8))*conductance;
      break;                   
  }
}

void printSignal(){
//  count++;
//  btSerial.print("#");
//  btSerial.print(count);
//  btSerial.print(". T: ");
//  btSerial.print(millis());
//  btSerial.print(", A= ");
  Serial.print("A: ");
  Serial.print(fsrA);
  Serial.print(", B:");
  Serial.print(fsrB);
  Serial.print(", C:");
  Serial.print(fsrC);  
  Serial.print(", D:");
  Serial.print(fsrD);
  Serial.print(", E:");
  Serial.print(fsrE);
  Serial.print(", F:");
  Serial.print(fsrF);
  Serial.println(";");
  
//  btSerial.print("CalA= ");
//  btSerial.print(calibration(fsrA, 'A'));
//  btSerial.print(", CalB= ");
//  btSerial.print(calibration(fsrB, 'B'));
//  btSerial.print(", CalC= ");
//  btSerial.print(calibration(fsrC, 'C'));
//  btSerial.print(", CalD= ");
//  btSerial.print(calibration(fsrD, 'D'));
//  btSerial.print(", CalE= ");
//  btSerial.print(calibration(fsrE, 'E'));
//  btSerial.print(", CalF= ");
//  btSerial.println(calibration(fsrF, 'F'));
  }


